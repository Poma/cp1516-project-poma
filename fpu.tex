\documentclass[a4paper]{article}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\title{Fermi-Pasta-Ulam model for non-linear oscillators}

\begin{document}
 
\maketitle

\section{Introduction}
The Fermi-Pasta-Ulam model deals with a certain number of oscillators with non-linear interactions, which prevent us from solving it analitically. Because of this problem, this model was the first one that was studied numerically with the first computers in the '50s. When they studied this model they thought that, exciting a single normal mode of the linear model, the system would reach to thermalization and the energy would be distributed in equal parts on each mode because of the classical Equipartion of Energy's theorem. Simulations showed that this didn't occur while a quasi-periodic regime was establishing, in which the energy passed through the modes but then returned periodically to the mode excited at the beginning. 

\section{Equations of the model}
\subsection{Linear case}
A system of N+2 oscillators with linear interaction and fixed boundary conditions is described by this $2^{nd}$ order system of N differential equations (with $m=k=1$) for the displacement from the equilibrium $q_{i}(t)$:
$$\ddot{q_{j}}(t)=q_{j+1}(t)-2\cdot q_{j}(t)+q_{j-1}(t) \;\;\; \forall j =1,...,N$$
with the boundary conditions $q_{0}(t)=0$ and $q_{N+1}(t)=0$.
This system can be trasformed in a system of 2N $1^{st}$ order differential equations:
$$\begin{cases} \dot{q}(t)=p(t) \\ \dot{p}(t)=q_{j+1}(t)-2\cdot q_{j}(t)+q_{j-1}(t) \end{cases}$$
In matrix form, defining the vector $^{t}(q_{1}(t),....,q_{N}(t),p_{1}(t),...,p_{N}(t))=(^{t}\mathbf{q}(t),^{t}\mathbf{p}(t))$, this system has the form $$\frac{d}{dt}\begin{pmatrix} \mathbf{q}(t) \\ \mathbf{p}(t)\end{pmatrix}=\begin{pmatrix} \mathbf{0} & \mathbf{1} \\ A & \mathbf{0} \end{pmatrix}\begin{pmatrix} \mathbf{q}(t) \\ \mathbf{p}(t)\end{pmatrix}$$ with $$A=\begin{pmatrix} -2 & 1 & .. & .. & .. \\ 1 & -2 & 1 & .. & ..  \\ 0 & 1 & -2 & 1 & .. & .. \\ .. & .. & .. & .. & .. & .. \\ .. & .. & .. & .. & 1 & -2 \end{pmatrix}$$
The $2^{nd}$ differential equations can be decoupled introducing the normal modes, defined as \cite{onofri} $$a_{k}=\sqrt{\frac{2}{N+1}}\sum\limits _{i=1}^{N} q_{i}(t)\sin (\frac{k\cdot h \cdot \pi}{N}) \;\; \forall k =1,...,N$$ 
In this new base the Hamiltonian can be expressed as the sum of N independent harmonic oscillators $$H=\sum\limits _{k=1}^{N} \frac{1}{2}(\dot{a}_{k}^2+ \omega _{k}^2 a_{k}^2)$$ where $\omega _{k}=2\sin (\frac{k\pi}{2(N+1)})$ are the eigenvalues of the matrix $A$, whose eigenvectors are the normal modes.

In this linear case, if we take as initial condition the excitation of the $1^{st}$ normal mode, then the energy will be forever distributed only in this mode while the energy of the other modes is 0. 

\subsection{Non-linear case}
Linear case described above can be modified in order to take in account a non-linear interaction between the masses, controlled by the parameter $\alpha$ . The equations for the displacement from the equilibrium $q_{i}(t)$ then become $$\ddot{q_{j}}(t)=[q_{j+1}(t)-2\cdot q_{j}(t)+q_{j-1}(t)]\cdot [1+\alpha (q_{i+1}(t)-q_{i-1}(t))] \;\;\; \forall j =1,...,N$$ with the same boundary conditions as in the linear case \cite{fpu}. 

This case can't be solved analitically so in the '50s Fermi, Pasta and Ulam used the first computers in order to solve numerically the equations of motion. This model would become the prototype for numerical studies. When they approached the problem they wanted to prove that the energy would follow the Equipartion theorem so, even if the system was initially excited in a single mode of the linear case, then the energy would be interchanged between the modes and, for long times, the temporal average of the energy would be the same for each mode (ergodic behavior). Numerical simulations instead proved the opposite: the energy passed through the modes but then turned to the first mode excited in a way that we can say 'quasi-periodic'. 


\section{Numerical methods}
To solve both cases I have written the N $2^{nd}$ order ODEs as a system of 2N $1^{st}$ order ODEs and then integrated the system with the Runge-Kutta 4 method. 

RK4 method follow this scheme: given the integration step $h$, $t_{n+1}=t_{n}+h$ while the solution of the equation of the system $\mathbf{y}_{n+1}=(y_{1,n+1},...,y_{N,n+1})$ at the $(n+1)^{th}$ step is $y_{i,n+1}=y_{i,n}+\frac{h}{6}(k_{1,i}+2k_{2,i}+2k_{3,i}+k_{4,i})$ where $$\begin{cases} k_{1,i}=f_{i}(y_{n},t_{n}) \\ k_{2,i}=f_{i}(t_{n}+\frac{h}{2},y_{n}+\frac{1}{2}k_{1}h) \\ k_{3,i}=f_{i}(t_{n}+\frac{h}{2},y_{n}+\frac{1}{2}k_{2}h) \\ k_{4,i}=f_{i}(t_{n}+h, y_{n}+k_{3}h) \end{cases}$$

The approximation of this method is $4^{th}$ order, i.e. $h^4$.


In the simulations I've used a chain of length $l=1$ with $N=32$ oscillating masses and two fixed at the extreme points. After putting them in equilibrium ($q_{0i}=\frac{i}{N+1}$), I've excited the first normal mode: $$\Delta q= \sin (\frac{\pi \cdot i }{N+1})$$


\section{Results}

Letting the system evolve with linear interaction we can see that the whole energy is in the first mode:

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.40]{lineare.png}
\caption{Linear case: energy for each mode}
\end{center}
\end{figure}


\newpage
The energy is conserved up to a factor $10^{-9}$ in , as we can see in this plot in which we can see the difference between the sum of the energy of all modes and the energy at the beginning, divided by the energy at the beginning. 


\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.30]{conservenergia.png}
\caption{Conservation of energy in the linear case}
\end{center}
\end{figure}




We can prove the convergence of the method with the autoconvergence, integrating the system with steps of $2\cdot h$, $h$ and $\frac{h}{2}$. With a $O(h^4)$ precision, we have that $\frac{y_{(2\cdot h)}-y_{(h)}}{y_{(h)}-y_{(h/2)}}=\frac{(2\cdot h)^4-(h)^{4}}{(h)^4-(\frac{h}{2})^{4}}=\frac{16-1}{1-\frac{1}{16}}=16=2^4$. This convergence can be showed overlapping the difference at the numerator with that one at the denominator multiplied for a factor $2^4$. In the next figure we have done this test with the energy of the system. We can see that there's a case of overconvergence because the overlapping is perfect with a factor $2^{5}$.  


\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.25\textheight]{autoconverg.png}
\caption{Testing the autoconvergence of the algorithm for the linear case}
\end{figure}
 
\newpage
In the non-linear case, letting the system evolve with different values of $\alpha$ we can see this trend for the energy of the first 6 or 10 modes:

\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.5\textheight]{fpu025.png}
\caption{FPU experiment with $\alpha =0.25$. We can see that the energy is passing through the modes but we can't see yet that there's no thermalization.}
\end{figure}

\newpage 
\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.5\textheight]{fpuparticular.png}
\caption{Particular of the previous figure in which we see that the energy, after about 160 periods of the first mode, returns to the first mode.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.5\textheight]{fpu05.png}
\caption{FPU experiment with $\alpha=0.5$.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.5\textheight]{fpu1.png}
\caption{FPU experiment with $\alpha=0.8$. More modes are excited }
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=1.1\columnwidth , height=0.5\textheight]{fpu3.png}
\caption{FPU experiment with $\alpha=3$. Plot of the first ten modes because superior modes are excited while $\alpha$ increases.}
\end{figure}

\newpage

\section{Conclusion}
In the previous graphics I have proved the different behaviour of the two cases. In particular:
\begin{itemize}
\item Linear case: I have proved the stability of the modes from initial conditions and the conservation of energy. I have also tested the convergence of the algorithm RK4 and seen that there's a case of overconvergence.

\item FPU case: I have reproduced the trend of the energy which each mode has in function of time for different values of $\alpha$ and showed that it doesn't reach a common average value because the first mode initially excited then will be excited again after about a period which depends on $\alpha$ (about 160 for $\alpha =0.25$, 110 for $\alpha =0.5$, 90 for $\alpha =0.8$ and 45 for $\alpha =3$). 
\end{itemize}



\begin{thebibliography}{1}

\bibitem{onofri}
E. Onofri
\newblock {\em Teoria degli operatori lineari}.
\newblock 1984.

\bibitem{fpu}
E. Fermi - J. Pasta - S. Ulam
\newblock {\em Studies of non linear problems }.
\newblock 1955.

\end{thebibliography}







\end{document}
