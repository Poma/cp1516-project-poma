#Studio numerico del modello di oscillatori con accoppiamento non lineare (di Fermi-Pasta-Ulam)

#prima parte: caso lineare
#equazione del moto (d/dt)^2[q_j]=q_(j+1)-2q_(j)+q_(j-1)  j=1..N e condizioni fisse al contorno

import numpy as np
import matplotlib.pyplot as plt
print "Oscillatori accoppiati di Fermi-Pasta-Ulam"
print "-----"
print "Metodo di Runge-Kutta per la soluzione del caso di oscillatori lineari"


tmax=1000 #tempo di integrazione

N=32 #numero di oscillatori
n=100000


def f(x,n):
	y=np.zeros([2*n])
	y[n]=(-2*x[0]+x[1])
	for i in range(1,n-1):
		y[n+i]=(x[i-1]-2*x[i]+x[i+1])
	y[2*n-1]=(-2*x[n-1]+x[n-2])
	for i in range(0,n):
		y[i]=x[n+i]
	return y


	
def RK4(t0,y0,tmax,nt,nosc):
	vy=np.zeros([2*nosc])
	vynew=np.zeros([2*nosc])
	deltat=(tmax-t0)/(nt+0.0)	
	vy=y0 #posizione e velocita' precedenti
	k1=deltat*f(vy[:],nosc)
	k2=deltat*f(vy[:]+0.5*k1,nosc)
	k3=deltat*f(vy[:]+0.5*k2,nosc)
	k4=deltat*f(vy[:]+k3,nosc)
	vynew[:]=vy[:]+1./6*(k1+2*k2+2*k3+k4)
	return vynew,deltat


x = np.linspace(0,1,N+2)
x1=x+np.sin(1*np.pi*x)
y=np.zeros([2*N])
ynew=np.zeros([2*N])
for i in range(1,N):
	ynew[i]=x1[i]-x[i]
plt.plot(x1,x1*0,'or')
plt.show()

omega=np.zeros(N)  #frequenze proprie
for i in range(N):
	omega[i]=2*np.sin((i+1)*np.pi/(2*(N+1)))


out_file1=open('tempi1.txt','w')
out_file2=open('energie1.txt','w')
for i in range(n):
	y,deltat=RK4(0,ynew,tmax,n,N)
	a=np.zeros((N)) #modi normali
	ap=np.zeros((N)) #derivate dei modi normali
	Energy=np.zeros((N))
	for k in range(N):
		for h in range(N):
			a[k]=a[k]+y[h]*np.sin((k+1)*(h+1)*np.pi/(N+1))
			ap[k]=ap[k]+y[h+N]*np.sin((k+1)*(h+1)*np.pi/(N+1))
		a[k]=a[k]*np.sqrt(2.0/(N+1))
		ap[k]=ap[k]*np.sqrt(2.0/(N+1))
	Energy[:]=0.5*(pow(ap[:],2)+pow(omega[:]*a[:],2))  #energie al tempo i per ogni modo
	t=i*deltat
	if (i%100==0):	
		out_file1.write('{0}\n'.format(t))
		for j in range(N-1):
			out_file2.write('{0}\t'.format(Energy[j]))
		out_file2.write('{0}\n'.format(Energy[N-1]))
	ynew=y
out_file1.close()
out_file2.close()

out_file3=open('tempi2.txt','w')
out_file4=open('energie2.txt','w')
for i in range(2*n):
	y,deltat=RK4(0,ynew,tmax,2*n,N)
	a=np.zeros((N)) #modi normali
	ap=np.zeros((N)) #derivate dei modi normali
	Energy=np.zeros((N))
	for k in range(N):	
		for h in range(N):
			a[k]=a[k]+y[h]*np.sin((k+1)*(h+1)*np.pi/(N+1))
			ap[k]=ap[k]+y[h+N]*np.sin((k+1)*(h+1)*np.pi/(N+1))
		a[k]=a[k]*np.sqrt(2.0/(N+1))
		ap[k]=ap[k]*np.sqrt(2.0/(N+1))
	Energy[:]=0.5*(pow(ap[:],2)+pow(omega[:]*a[:],2))  #energie al tempo i per ogni modo
	t=i*deltat
	if (i%100==0):	
		out_file3.write('{0}\n'.format(t))
		for j in range(N-1):
			out_file4.write('{0}\t'.format(Energy[j]))
		out_file4.write('{0}\n'.format(Energy[N-1]))
	ynew=y
out_file3.close()
out_file4.close()
'''
out_file5=open('tempi4.txt','w')
out_file6=open('energie4.txt','w')
for i in range(4*n):
	y,deltat=RK4(0,ynew,tmax,4*n,N)
	a=np.zeros((N)) #modi normali
	ap=np.zeros((N)) #derivate dei modi normali
	Energy=np.zeros((N))
	for k in range(N):	
		for h in range(N):
			a[k]=a[k]+y[h]*np.sin((k+1)*(h+1)*np.pi/(N+1))
			ap[k]=ap[k]+y[h+N]*np.sin((k+1)*(h+1)*np.pi/(N+1))
		a[k]=a[k]*np.sqrt(2.0/(N+1))
		ap[k]=ap[k]*np.sqrt(2.0/(N+1))
	Energy[:]=0.5*(pow(ap[:],2)+pow(omega[:]*a[:],2))  #energie al tempo i per ogni modo
	t=i*deltat
	if (i%100==0):	
		out_file5.write('{0}\n'.format(t))
		for j in range(N-1):
			out_file6.write('{0}\t'.format(Energy[j]))
		out_file6.write('{0}\n'.format(Energy[N-1]))
	ynew=y
out_file5.close()
out_file6.close()
'''
out_file7=open('tempi05.txt','w')
out_file8=open('energie05.txt','w')
for i in range(n/2):
	y,deltat=RK4(0,ynew,tmax,n/2,N)
	a=np.zeros((N)) #modi normali
	ap=np.zeros((N)) #derivate dei modi normali
	Energy=np.zeros((N))
	for k in range(N):	
		for h in range(N):
			a[k]=a[k]+y[h]*np.sin((k+1)*(h+1)*np.pi/(N+1))
			ap[k]=ap[k]+y[h+N]*np.sin((k+1)*(h+1)*np.pi/(N+1))
		a[k]=a[k]*np.sqrt(2.0/(N+1))
		ap[k]=ap[k]*np.sqrt(2.0/(N+1))
	Energy[:]=0.5*(pow(ap[:],2)+pow(omega[:]*a[:],2))  #energie al tempo i per ogni modo
	t=i*deltat
	if (i%100==0):	
		out_file7.write('{0}\n'.format(t))
		for j in range(N-1):
			out_file8.write('{0}\t'.format(Energy[j]))
		out_file8.write('{0}\n'.format(Energy[N-1]))
	ynew=y
out_file7.close()
out_file8.close()


	
#seconda parte: caso non lineare
#equazione del moto (d/dt)^2[q_j]=(q_(j+1)-2q_(j)+q_(j-1))*(1+alpha(q_(j+1)-q_(j-1)))  j=1..N e condizioni fisse al contorno

print "-----"
print "Metodo di Runge-Kutta per la soluzione del caso di oscillatori con interazione quadratica"
alpha=0.25

def g(x,n,alpha):
	y=np.zeros([2*n])
	y[n]=(-2*x[0]+x[1])*(1+alpha*x[1])
	for i in range(1,n-1):
		y[n+i]=(x[i-1]-2*x[i]+x[i+1])*(1+alpha*(x[i+1]-x[i-1]))
	y[2*n-1]=(-2*x[n-1]+x[n-2])*(1-alpha*x[n-2])
	for i in range(0,n):
		y[i]=x[n+i]
	return y



def RK4fpu(t0,y0,tmax,nt,nosc,alpha,A):
	vy=np.zeros([2*nosc])
	vynew=np.zeros([2*nosc])
	deltat=(tmax-t0)/(nt+0.0)
	vy=y0 #posizione e velocita' precedenti
	k1=deltat*g(vy,nosc,alpha)
	k2=deltat*g(vy+0.5*k1,nosc,alpha)
	k3=deltat*g(vy+0.5*k2,nosc,alpha)
	k4=deltat*g(vy+k3,nosc,alpha)
	vynew=vy+1./6*(k1+2*k2+2*k3+k4)
	return vynew,deltat

x = np.linspace(0,1,N+2)
x1=x+np.sin(np.pi*x)
y0=np.zeros([2*N])
ynew=np.zeros([2*N])
for i in range(0,N):
	ynew[i]=x1[i+1]-x[i+1]
plt.plot(x1,x1*0,'or')
plt.show()

	
out_filefput=open('tempifpu.txt','w')
out_filefpuE=open('energiefpu.txt','w')
for i in range(n):
	y,deltat=RK4fpu(0,ynew,tmax,n,N,alpha,A)
	a=np.zeros((N)) #modi normali
	ap=np.zeros((N)) #derivate dei modi normali
	Energy=np.zeros((N))
	for k in range(N):
		for h in range(N):
			a[k]=a[k]+y[h]*np.sin((k+1.0)*(h+1)*np.pi/(N+1))
			ap[k]=ap[k]+y[h+N]*np.sin((k+1.0)*(h+1)*np.pi/(N+1))
		a[k]=a[k]*np.sqrt(2.0/(N+1))
		ap[k]=ap[k]*np.sqrt(2.0/(N+1))
	for j in range(N):
		Energy[j]=0.5*(pow(ap[j],2)+pow(omega[j]*a[j],2))  #energie al tempo i per ogni modo
	t=i*deltat
	if (i%100==0):	
		out_filefput.write('{0}\n'.format(t))
		for j in range(9):
			out_filefpuE.write('{0}\t'.format(Energy[j]))
		out_filefpuE.write('{0}\n'.format(Energy[9]))
	ynew=y
out_filefput.close()
out_filefpuE.close()


