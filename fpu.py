#Studio numerico del modello di oscillatori con accoppiamento non lineare (di Fermi-Pasta-Ulam)

#prima parte: caso lineare
#equazione del moto (d/dt)^2[q_j]=q_(j+1)-2q_(j)+q_(j-1)  j=1..N e condizioni fisse al contorno

import numpy as np
import matplotlib.pyplot as plt
print "Oscillatori accoppiati di Fermi-Pasta-Ulam"
print "-----"
print "Metodo di Runge-Kutta per la soluzione del caso di oscillatori lineari"


tmax=200 #tempo di integrazione

N=64  #numero di oscillatori
n=2000


def f(x,n,A):	
	return np.dot(A,x)


	
def RK4(t0,y0,tmax,nt,nosc,A):
	vt=np.zeros([nt])
	vy=np.zeros([2*nosc,nt])
	deltat=(tmax-t0)/(nt+0.0)
	vt[0]=t0  #istante iniziale	
	vy[:,0]=y0 #posizione e velocita' iniziali
	for i in range(1,nt):
		vt[i]=t0+i*deltat
		k1=deltat*f(vy[:,i-1],nosc,A)
		k2=deltat*f(vy[:,i-1]+0.5*k1,nosc,A)
		k3=deltat*f(vy[:,i-1]+0.5*k2,nosc,A)
		k4=deltat*f(vy[:,i-1]+k3,nosc,A)
		vy[:,i]=vy[:,i-1]+1./6*(k1+2*k2+2*k3+k4)
	return vt,vy

A=np.zeros([2*N,2*N])
for i in range(1,N-1):
	A[N+i,i]=-2
	A[N+i,i-1]=1	
	A[N+i-1,i]=1
	A[i,N+i]=1
A[N,0]=-2
A[N,1]=1
A[2*N-1,N-1]=-2
A[2*N-1,N-2]=1
A[0,N]=1
A[N-1,2*N-1]=1

x = np.linspace(0,1,N+2)
x1=x+0.1*np.sin(1*np.pi*x)  
y0=np.zeros([2*N])
for i in range(1,N):
	y0[i]=x1[i]-x[i]
plt.plot(x1,x1*0,'or')
plt.show()
t,y=RK4(0,y0,tmax,n,N,A)
t1,y1=RK4(0,y0,tmax,2*n,N,A)
t2,y2=RK4(0,y0,tmax,4*n,N,A)
for i in range(0,N):
	plt.plot(t,y[i,:],label=i+1)
plt.legend()
plt.show()

omega=np.zeros(N)  #frequenze proprie
for i in range(N):
	omega[i]=2*np.sin((i+1)*np.pi/(2*(N+1)))
a=np.zeros((n,N)) #modi normali
a1=np.zeros((2*n,N))
a2=np.zeros((4*n,N))
ap=np.zeros((n,N)) #derivate dei modi normali
ap1=np.zeros((2*n,N))
ap2=np.zeros((4*n,N))
for i in range(N):
	a[0,0]=a[0,0]+y[0,i]*np.sin((i+1)*np.pi/(N))
	a1[0,0]=a1[0,0]+y1[0,i]*np.sin((i+1)*np.pi/(N))
	a2[0,0]=a2[0,0]+y2[0,i]*np.sin((i+1)*np.pi/(N))
a[0,0]=a[0,0]*np.sqrt(2.0/(N+1))
a1[0,0]=a1[0,0]*np.sqrt(2.0/(N+1))
a2[0,0]=a2[0,0]*np.sqrt(2.0/(N+1))

Energy=np.zeros((n,N))
Energy[0,0]=0.5*(0.1*omega[0]*a[0,0])**2
Energy1=np.zeros((2*n,N))
Energy1[0,0]=0.5*(0.1*omega[0]*a1[0,0])**2
Energy2=np.zeros((4*n,N))
Energy2[0,0]=0.5*(0.1*omega[0]*a2[0,0])**2
	
for i in range(1,n):
	for k in range(N):	
		for h in range(N):
			a[i,k]=a[i,k]+y[h,i]*np.sin((k+1)*(h+1)*np.pi/(N))
			ap[i,k]=ap[i,k]+y[h+N,i]*np.sin((k+1)*(h+1)*np.pi/(N))
		a[i,k]=a[i,k]*np.sqrt(2.0/(N+1))
		ap[i,k]=ap[i,k]*np.sqrt(2.0/(N+1))
	Energy[i,:]=0.5*(pow(ap[i,:],2)+pow(omega[:]*a[i,:],2))  #energie al tempo i per ogni modo

for i in range(1,2*n):
	for k in range(N):	
		for h in range(N):
			a1[i,k]=a1[i,k]+y1[h,i]*np.sin((k+1)*(h+1)*np.pi/(N))
			ap1[i,k]=ap1[i,k]+y1[h+N,i]*np.sin((k+1)*(h+1)*np.pi/(N))
		a1[i,k]=a1[i,k]*np.sqrt(2.0/(N+1))
		ap1[i,k]=ap1[i,k]*np.sqrt(2.0/(N+1))
	Energy1[i,:]=0.5*(pow(ap1[i,:],2)+pow(omega[:]*a1[i,:],2))  #energie al tempo i per ogni modo

for i in range(1,4*n):
	for k in range(N):	
		for h in range(N):
			a2[i,k]=a2[i,k]+y2[h,i]*np.sin((k+1)*(h+1)*np.pi/(N))
			ap2[i,k]=ap2[i,k]+y2[h+N,i]*np.sin((k+1)*(h+1)*np.pi/(N))
		a2[i,k]=a2[i,k]*np.sqrt(2.0/(N+1))
		ap2[i,k]=ap2[i,k]*np.sqrt(2.0/(N+1))
	Energy2[i,:]=0.5*(pow(ap2[i,:],2)+pow(omega[:]*a2[i,:],2))  #energie al tempo i per ogni modo

for i in range(5):
	plt.plot(t,Energy[:,i],label=i+1)
plt.legend()
plt.show()
Energytot=np.zeros([n])
Energytot1=np.zeros([2*n])
Energytot2=np.zeros([4*n])
for i in range(N):
	Energytot=Energytot+Energy[:,i]
	Energytot1=Energytot1+Energy1[:,i]
	Energytot2=Energytot2+Energy2[:,i]
plt.plot(t,abs(Energytot-Energytot1[::2]))
plt.plot(t1,32*abs(Energytot1-Energytot2[::2]))
plt.show()
I=np.ones([n])
DeltaE=(-Energytot[2]*I+Energytot)/Energytot[2]
plt.plot(t,DeltaE)
plt.show()


	
#seconda parte: caso non lineare
#equazione del moto (d/dt)^2[q_j]=(q_(j+1)-2q_(j)+q_(j-1))*(1+alpha(q_(j+1)-q_(j-1)))  j=1..N e condizioni fisse al contorno

print "-----"
print "Metodo di Runge-Kutta per la soluzione del caso di oscillatori con interazione quadratica"
alpha=0.5
def g(x,n,A,alpha):
	y=np.dot(A,x)
	y[0]=y[0]*(1+alpha*x[1])
	for i in range(1,x.shape[0]-1):
		y[i]=y[i]*(1+alpha*(x[i+1]-x[i-1]))
	y[x.shape[0]-1]=y[x.shape[0]-1]*(1+alpha*x[x.shape[0]-2])
	return y

def RK41(t0,y0,tmax,nt,nosc,alpha,A):
	vt=np.zeros([nt])
	vy=np.zeros([2*nosc,nt])
	deltat=(tmax-t0)/(nt+0.0)
	vt[0]=t0  #istante iniziale	
	vy[:,0]=y0 #posizione e velocita' iniziali
	for i in range(1,nt):
		vt[i]=t0+i*deltat
		k1=deltat*g(vy[:,i-1],nosc,A,alpha)
		k2=deltat*g(vy[:,i-1]+0.5*k1,nosc,A,alpha)
		k3=deltat*g(vy[:,i-1]+0.5*k2,nosc,A,alpha)
		k4=deltat*g(vy[:,i-1]+k3,nosc,A,alpha)
		vy[:,i]=vy[:,i-1]+1./6*(k1+2*k2+2*k3+k4)
	return vt,vy

x = np.linspace(0,1,N+2)
x1=x+0.1*np.sin(np.pi*x)
y0=np.zeros([2*N])
for i in range(1,N):
	y0[i]=x1[i]-x[i]
plt.plot(x1,x1*0,'or')
plt.show()
t,y=RK41(0,y0,tmax,n,N,alpha,A)

#for i in range(0,N):
#	plt.plot(t,y[i,:],label=i+1)
#plt.legend()
#plt.show()

omega=np.zeros(N)  #frequenze proprie
for i in range(N):
	omega[i]=2*np.sin((i+1)*np.pi/(2*(N+1)))
print omega[0]
a=np.zeros((n,N)) #modi normali
ap=np.zeros((n,N)) #derivate dei modi normali

for i in range(N):
	a[0,0]=a[0,0]+y[0,i]*np.sin((i+1)*np.pi/(N))
a[0,0]=a[0,0]*np.sqrt(2.0/(N+1))
Energy=np.zeros((n,N))
Energy[0,0]=0.5*(0.1*omega[0]*a[0,0])**2

	
for i in range(1,n):
	for k in range(N):	
		for h in range(N):
			a[i,k]=a[i,k]+y[h,i]*np.sin((k+1)*(h+1)*np.pi/(N))
			ap[i,k]=ap[i,k]+y[h+N,i]*np.sin((k+1)*(h+1)*np.pi/(N))
		a[i,k]=a[i,k]*np.sqrt(2.0/(N+1))
		ap[i,k]=ap[i,k]*np.sqrt(2.0/(N+1))
	Energy[i,:]=0.5*(pow(ap[i,:],2)+pow(omega[:]*a[i,:],2))  #energie al tempo i per ogni modo


for i in range(6):
	plt.plot(t,Energy[:,i],label=i+1)
plt.legend()
plt.title("Energia dei modi normali")
plt.show()




