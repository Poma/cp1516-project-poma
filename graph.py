import numpy as np
from matplotlib import pyplot as plt
#graphics

f1=open('energie1.txt','r').read()
f2=open('energie2.txt','r').read()
f3=open('energie05.txt','r').read()
f4=open('tempi1.txt','r').read()
f5=open('tempi2.txt','r').read()
f6=open('tempi05.txt','r').read()
f7=open('energiefpu3.txt','r').read()
f8=open('tempifpu3.txt','r').read()


n=100000
N=32
'''
#linear time-step h
f1=f1.replace("\t"," ")
f1=f1.replace("\n"," ")
f1=f1.split(" ")
E1=np.zeros([n/100,N])
for i in range(N):
	for j in range(n/100):
		E1[j,i]=float(f1[32*j+i])
f4=f4.replace("\n"," ")
f4=f4.split(" ")
t1=np.zeros([n/100])
for i in range(n/100):
	t1[i]=float(f4[i])
for i in range(6):
	plt.plot(2*np.sin(np.pi/66)*t1/(2*np.pi),E1[:,i],label=i+1)
plt.title("Energies of the first 6 modes")
plt.xlabel("Periods of the first mode")
plt.ylabel("Energy")
plt.legend()
plt.show()

Etot1=np.zeros(n/100)
for i in range(N):
	Etot1=Etot1+E1[:,i]
plt.plot(t1/(2*np.pi)*2*np.sin(np.pi/66),(Etot1[0]-Etot1)/Etot1[0])
plt.title("Conservation of energy")
plt.xlabel("Periods of the first mode")
plt.ylabel("(E(0)-E(t))/E(0)")
plt.legend()
plt.show()

#linear time-step h/2
f2=f2.replace("\t"," ")
f2=f2.replace("\n"," ")
f2=f2.split(" ")
E2=np.zeros([2*n/100,N])
for i in range(N):
	for j in range(2*n/100):
		E2[j,i]=float(f2[32*j+i])
f5=f5.replace("\n"," ")
f5=f5.split(" ")
t2=np.zeros([2*n/100])
for i in range(2*n/100):
	t2[i]=float(f5[i])
for i in range(6):
	plt.plot(t2/(2*np.pi)*2*np.sin(np.pi/66),E2[:,i],label=i+1)
plt.title("Energies of the first 6 modes")
plt.xlabel("Periods of the first mode")
plt.ylabel("Energy")
plt.legend()
plt.show()

Etot2=np.zeros(2*n/100)
for i in range(N):
	Etot2=Etot2+E2[:,i]
plt.plot(t2/(2*np.pi)*2*np.sin(np.pi/66),(Etot2[0]-Etot2)/Etot2[0])
plt.title("Conservation of energy")
plt.xlabel("Periods of the first mode")
plt.ylabel("(E(0)-E(t))/E(0)")
plt.legend()
plt.show()

#linear time-step 2h
f3=f3.replace("\t"," ")
f3=f3.replace("\n"," ")
f3=f3.split(" ")
E3=np.zeros([n/200,N])
for i in range(N):
	for j in range(n/200):
		E3[j,i]=float(f3[32*j+i])
f6=f6.replace("\n"," ")
f6=f6.split(" ")
t3=np.zeros([n/200])
for i in range(n/200):
	t3[i]=float(f6[i])
for i in range(6):
	plt.plot(t3/(2*np.pi)*2*np.sin(np.pi/66),E3[:,i],label=i+1)
plt.title("Energies of the first 6 modes")
plt.xlabel("Periods of the first mode")
plt.ylabel("Energy")
plt.legend()
plt.show()

Etot3=np.zeros(n/200)
for i in range(N):
	Etot3=Etot3+E3[:,i]
plt.plot(t3/(2*np.pi)*2*np.sin(np.pi/66),(Etot3[0]-Etot3)/Etot3[0])
plt.title("Conservation of energy")
plt.xlabel("Periods of the first mode")
plt.ylabel("(E(0)-E(t))/E(0)")
plt.legend()
plt.show()

#autoconvergence
plt.plot(t3,abs(Etot1[::2]-Etot3[:]),label="Etot(2h)-Etot(h)")
plt.plot(t1,32*(Etot2[::2]-Etot1[:]),label="Etot(h)-Etot(h/2)")
plt.title("Autoconvergence ")
plt.xlabel("Time")
plt.ylabel("Energy")
plt.legend()
plt.show()
'''
#fpu
n=100000
f7=f7.replace("\t"," ")
f7=f7.replace("\n"," ")
f7=f7.split(" ")
Efpu=np.zeros([n/100,10])
for i in range(10):
	for j in range(n/100):
		Efpu[j,i]=float(f7[10*j+i])
f8=f8.replace("\n"," ")
f8=f8.split(" ")
tfpu=np.zeros([n/100])
for i in range(n/100):
	tfpu[i]=float(f8[i])
for i in range(10):
	plt.plot(2*np.sin(np.pi/66)*tfpu/(2*np.pi),Efpu[:,i],label=i+1)
plt.title("Energy of the first 10 modes")
plt.xlabel("Periods of the first mode")
plt.ylabel("Energy")
plt.legend()
plt.show()
Etot=np.zeros([n/100])
for i in range(10):
	Etot[:]=Etot[:]+Efpu[:,i]
plt.plot(2*np.sin(np.pi/66)*tfpu/(2*np.pi),Etot)
plt.xlabel("Periods of the first mode")
plt.ylabel("Sum of the energy of the first 10 modes")
plt.show()


